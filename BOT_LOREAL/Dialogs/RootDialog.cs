﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BOT_LOREAL.Properties;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BOT_LOREAL.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        //Menu Institucional
        private const string QuemSomosOption = "Quem somos?";
        private const string QueFazemosOption = "O que fazemos?";

        //Opções SIM | NAO
        private const string OptionSim = "Sim";
        private const string OptionNao = "Nao";
        
        private const string QueroSimOption = "Quero voltar ao menu principal";
        private const string QueroNaoOption = "Não, obrigado";

        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            await context.PostAsync("Olá eu sou a Lóri, o chatbot da L'Oréal e estou aqui para te ajudar. \n\n Escolha uma categoria abaixo para começarmos ");
            var reply = context.MakeMessage();
            Thread.Sleep(1000);

            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardAttachments();
            await context.PostAsync(reply);
            context.Wait(this.OnOptionSelected);
        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if(message.Text.ToLower().Contains("institucional"))
                {
                PromptDialog.Choice(
                    context,
                    this.AfterChoiceSelected,
                    new[] { QuemSomosOption, QueFazemosOption },
                    "**Selecione uma das opções abaixo:**",
                    "Desculpe, mas preciso que escolha uma das opções abaixo.",
                    attempts: 2);
                }
        }

        private async Task AfterChoiceSelected(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var selection = await result;
                var reply = context.MakeMessage();

                switch (selection)
                {
                    case QuemSomosOption:
                        await context.PostAsync(Resource.QuemSomos1);
                        Thread.Sleep(1500);
                        await context.PostAsync(Resource.QuemSomos2);
                        Thread.Sleep(1500);
                        await context.PostAsync(Resource.QuemSomos3);
                        await PromptChoiceAsync(context, Resource.VoltarMenu);
                        break;

                    case QueFazemosOption:
                        await context.PostAsync(Resource.OQueFazemos);
                        await PromptChoiceAsync(context, Resource.VoltarMenu);
                        break;

                    case OptionSim:
                        PromptDialog.Choice(
                            context,
                            this.AfterChoiceSelected,
                            new[] { QuemSomosOption, QueFazemosOption},
                            "**Selecione uma das opções abaixo:**",
                            "Desculpe, mas preciso que você escolha uma das opções abaixo.",
                            attempts: 2);
                        break;

                    case OptionNao:
                        PromptDialog.Choice(
                        context,
                        this.AfterChoiceSelected,
                        new[] { QueroSimOption, QueroNaoOption },
                        "**Quer que eu te ajude com mais alguma coisa?**",
                        "Desculpe, mas preciso que você escolha uma das opções abaixo.",
                        attempts: 2);
                        break;

                    case QueroSimOption:
                        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                        reply.Attachments = GetCardAttachments();
                        await context.PostAsync(reply);
                        context.Wait(this.OnOptionSelected);
                        break;

                    case QueroNaoOption:
                        await context.PostAsync(Resource.Despedida);
                        await this.StartAsync(context);
                        break;
                }
            }
            catch (TooManyAttemptsException)
            {

                await this.StartAsync(context);
            }
        }

        private static IList<Attachment> GetCardAttachments()
        {
            return new List<Attachment>()
            {
                GetHeroCard(
                    "Institucional",
                    "L'Oreal Paris é uma marca francesa na vanguarda das marcas de beleza global.",
                    "Aqui eu posso te falar sobre a L'Oréal.",
                    new CardImage(url:"https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_Loreal/menu_institucional.png"),
                    new CardAction(ActionTypes.ImBack, "Institucional", value: "Institucional")),
            };
        }

        private static Attachment GetHeroCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)
        {
            var heroCard = new HeroCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
                Images = new List<CardImage>() { cardImage },
                Buttons = new List<CardAction>() { cardAction },
            };

            return heroCard.ToAttachment();
        }
        public async Task PromptChoiceAsync(IDialogContext context, string texto)
        {
            PromptDialog.Choice(
            context,
            this.AfterChoiceSelected,
            new[] { OptionSim, OptionNao },
            texto,
            "Me desculpe, mas eu preciso que você responda se eu posso ou não te ajudar com algum outro problema:",
            attempts: 2);
        }
    }
}